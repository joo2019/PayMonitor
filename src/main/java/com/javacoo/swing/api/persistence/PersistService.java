package com.javacoo.swing.api.persistence;

import com.javacoo.xkernel.spi.Spi;

import java.util.List;
import java.util.Optional;

/**
 * 数据持久化
 * <p>说明:</p>
 * <li></li>
 *
 * @author DuanYong
 * @version 1.0
 * @since 2019/8/30 9:59
 */
@Spi("default")
public interface PersistService<T> {
    /**
     * 新增数据
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 10:12
     * @param parameterObject 参数对象
     * @return int 影响记录行数
     * @version 1.0
     */
    int insert(final T parameterObject);
    /**
     *  更新数据
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 10:13
     * @param parameterObject 参数对象
     * @return int 影响记录行数
     * @version 1.0
     */
    int update(final T parameterObject);
    /**
     * 删除数据
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 10:13
     * @param parameterObject 参数对象
     * @return int 影响记录行数
     * @version 1.0
     */
    int delete(final T parameterObject);
    /**
     * 查询单个对象
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 10:14
     * @param parameterObject 参数对象
     * @return Object 返回唯一一条满足条件的结果对象
     * @version 1.0
     */
    T get(final T parameterObject);
    /**
     * 查询对象集合
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 10:15
     * @param parameterObject 参数对象
     * @return List<?> 满足条件的对象集合
     * @version 1.0
     */
    Optional<List<T>> getList(final T parameterObject);

}
