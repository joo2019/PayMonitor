package com.javacoo.swing.core.monitor;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;

import com.javacoo.swing.api.data.DataHandler;
import com.javacoo.swing.core.constant.Constant;
import com.javacoo.xkernel.spi.ExtensionLoader;
import com.teamdev.jxbrowser.chromium.BeforeURLRequestParams;
import com.teamdev.jxbrowser.chromium.BytesData;
import com.teamdev.jxbrowser.chromium.DataReceivedParams;
import com.teamdev.jxbrowser.chromium.FormData;
import com.teamdev.jxbrowser.chromium.LoadURLParams;
import com.teamdev.jxbrowser.chromium.MultipartFormData;
import com.teamdev.jxbrowser.chromium.NetworkDelegate;
import com.teamdev.jxbrowser.chromium.TextData;
import com.teamdev.jxbrowser.chromium.UploadData;
import com.teamdev.jxbrowser.chromium.UploadDataType;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

/**
 * 我的账单监控
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 10:37
 * @Version 1.0
 */
public class AliPayMyBillRecordMonitorWindow extends BaseAliPayMonitorWindow {
     /**
     * 消费详细
     */
    private static final String ACCOUNT_DETAIL_URL = "https://consumeprod.alipay.com/record/standard.htm";

    public AliPayMyBillRecordMonitorWindow(){
        super();
        dataHandler = ExtensionLoader.getExtensionLoader(DataHandler.class).getExtension(Constant.MONITOR_WINDOW_TYPE_MY_BILL);
    }
    /**
     * 获取加载URL参数
     * <li></li>
     *
     * @author duanyong@jccfc.com
     * @date 2020/9/15 17:09
     * @return: com.teamdev.jxbrowser.chromium.LoadURLParams
     */
    @Override
    protected LoadURLParams getLoadURLParams() {
        return new LoadURLParams(ACCOUNT_DETAIL_URL);
    }
    @Override
    protected void onFinishLoaded(){
        //处理HTML
        dataHandler.handle(browser.getHTML());
    }

    @Override
    protected NetworkDelegate getNetworkDelegate() {
        return new MyNetworkDelegate(){
            @Override
            public void onBeforeURLRequest(BeforeURLRequestParams params){
                if ("POST".equals(params.getMethod()) && params.getURL().contains(dataHandler.getMonitorUrl())) {
                    UploadData uploadData = params.getUploadData();
                    UploadDataType dataType = uploadData.getType();
                    if (dataType == UploadDataType.FORM_URL_ENCODED) {
                        FormData data = (FormData) uploadData;
                        data.setPair("query-pageNum", "2");
                    } else if (dataType == UploadDataType.MULTIPART_FORM_DATA) {
                        MultipartFormData data = (MultipartFormData) uploadData;
                        // data.setPair("key1", "value1", "value2");
                        // data.setPair("key2", "value2");
                        // data.setFilePair("file3", "C:\\Test.zip");
                    } else if (dataType == UploadDataType.PLAIN_TEXT) {
                        // TextData data = (TextData) uploadData;
                        // data.setText("My data");
                    } else if (dataType == UploadDataType.BYTES) {
                        // BytesData data = (BytesData) uploadData;
                        // data.setData("My data".getBytes());
                    }
                    // Apply modified upload data that will be sent to a web server.
                    params.setUploadData(uploadData);
                }
            }
        };
    }

    ;
}
